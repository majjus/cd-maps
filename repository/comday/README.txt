434 Fort Wars by Whiskers434:

Welcome to 434 Fort Wars a new and exciting PvP map inspired by Dewtroid�s Airship Battle Map.
Fort Wars is intended to be played with around 16 to 32 people, i.e. 8v8 to 16v16.

Please take some time to read the rules and objectives.

Objective:

The Objective of 434 Fort Wars is to destroy the enemy teams flag, fully and completely, by any means necessary including but not limited to:

* Breaking the Blocks
* Setting Fire to the Flag
* TNT Cannon 
* Lava

Only the wool needs to be destroyed for a victory. The Flagpost is not required for a win.
N.B// each flag consists of blue, red and black wool. Each team has their team colour wool within the castle and it is acceptable to repair a flag with wool from within your castle and/or wool you can steal from the enemies castle or flag.

Rules:

1. There is no limit to the actions players can do to other players - no holds bar PvP.
2. Spawn is a safe zone! - No fighting or PvP actions are allowed to take place anywhere including      the Rule Board and the Bridges.
3. Team Hats must be worn at all times. Team hats are used to identify your team and its members. The Red Team wear Golden Hats and the Blue team wear Chain Hats. No player is allowed onto the killing zone without a hat on. If beds are used there is a chest of helmets available within the castles themselves.
4. Players must enter the killing zone via their spawn drops only or beds placed in their castles and leaving the bedrock area is not allowed!

Set-Up:

If Spawn Protection is in effect then use the initial starting spawn location. If you do not use a Spawn Protection plugin (or whatever) then feel free to move Spawn forward to the rule board area.

To begin the games give each player a chance to read the rules and hints. Once players are divided into teams unlock the fence by pulling the first lever. Tell the players to put on their appropriate hat and get ready to start. Once you are ready pull the second lever and allow the players to jump down.  
It is possible to lose on occasion half a heart from fall damage though players should start the game with both full health and a full hunger bar so this should not be a problem.

If you have any questions or queries please post them on the forum,

Whiskers434 

(written by Oddmast)
