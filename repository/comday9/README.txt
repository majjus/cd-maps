I Don't Oven

This map is designed exclusively for multiplayer. Recommended players is 16-32


HOW TO PLAY:

The map contains two identical ovens, each oven contains an obsidian core that
cooks the chicken. The goal is to defend your core while attempting to cause
your enemy's core to leak lava into the void.

To start a game, divide players into two teams, and set your team's switch to
'READY'. Once both sides are ready the gate will open.

Players may build or craft whatever they can with the resources available.

Players may not build in, on, directly next to, or behind, the spawn platform
or spawn tunnels.

Players may not fight in, or on the spawn platform, or spawn tunnels.

If a player respawns on the spawn platform they must immediately proceed to their oven.

Players may not use a bucket on lava.

Players may plug up their core prior to lava leaking below the bottom of the
ship

Players should not damage their own core.

Players should use gold or chain helms to identify teams unless other team
identification methods are used.



Recommended server.properties options
----------------------
allow-nether=false
allow-flight=true
spawn-monsters=false
pvp=true
difficulty=3
gamemode=0
max-players=32
----------------------

allow-flight is enabled to prevent people from getting kicked when they fall
into the void.


I Don't Oven By DJQJ


Spawn plarform By Dewtroid

Special thanks to Dewtroid for the inspiration to make this map.