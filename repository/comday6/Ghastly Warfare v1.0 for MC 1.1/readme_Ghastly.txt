Welcome to....

Ghastly Warfare v1.0 for MC 1.1

Before playing, there are a few things you should know.

� This is my first map, so the edges may be a bit rough.
� There is no PvE in this map, so turning monsters off might help.
� Optionally, there is one squid that you may spawn in the Aquarium (he will help you referee), so if
you like, you may turn on Passive Mobs/Animals.

I believe that's it, thanks for playing the map and be sure to leave specific constructive feedback
whereever and whenever necessary. Thanks!