Space Ship Battles v1.0
By Oversoul96

Setup: 
 -Monsters: Off
 -PvP: On
 -Equal teams of 8v8 - 16v16

--All of the following information can be found on the rules board in-game--

Objectives:
 -Destroy the enemy core by making the lava leak beneath the ship, or by removing all of the lava source blocks. There are three source blocks in total.
 -Destroy the two antennas that are on the enemy ship to disable enemy communications. Antennas count as destroyed when all ores -lapis or redstone- are destroyed.

*Both objectives must be completed to win. They do not have to be done in any specific order*

Rules:
 -Players may not build in or above the spawn pools on the ships.
 -Players may not fight or build in the spawn tunnels.
 -Players may not go through the enemy's spawn tunnel.
 -Players may collect materials from the ships.
 -Players may not use buckets on lava.
 -Players may not destroy their own core or antennas.
 -Players may barricade their core or antennas.
 -Players must wear their team identification helmets at all times. [Red: Butter Heads: Gold Helmets] [Blue: Fish Nets: Chain Helmets]

Other:

The map is in a snow biome to simulate the freezing cold atmosphere of space. Water will freeze if it is open to the sky or does not have a light source nearby.

Ship Names:
 -Blue Ship: Star Fish
 -Red Ship: Space Biscuit

Recommendations/Tips:
 -Keep persistent night time throughout the map for the feeling of outer space.
 -Engines are explosive.
 -Engines contain diamond. Iron can be found all around the ship.


(This map is not meant to be realistic. Gravity, oxygen, and the vacuum of space are not affected)

Installation:
 -Rename "Space Ship Battles v1.0" folder to "world", and extract the folder into your minecraft server folder.